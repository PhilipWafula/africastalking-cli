import logging

import africastalking
import phonenumbers
from phonenumbers.phonenumberutil import NumberParseException

logg = logging.getLogger(__name__)

def process_args(argparser):
    argparser.add_argument('-s', '--sender-id', dest='s', type=str, help='sender id for outgoing sms')
    argparser.add_argument('-r', '--recipient', dest='r', action='append', type=str, help='recipient list, comma-separated')
    argparser.add_argument('-f', '--recipient-file', dest='f', type=str, help='load recipients from file (one number per line)')
    argparser.add_argument('message', type=str, help='sms message content')


def validate_args(args):
    if len(args.message) > 160:
        raise ValueError('SMS longer than 160 characters')
    if args.r != None and args.f != None:
        raise ValueError('Cannot supply both -f and -r flags')


def validate_phone(n, country=None):
    return phonenumbers.parse(n, country)

def execute(config, args):
    c = africastalking.SMS

    sender_id = config.get('AFRICASTALKING_SENDER_ID')
    if args.s != None:
        sender_id = args.s

    recipients = []
    if args.f != None:
        logg.debug('reading recipients from file {}'.format(args.f))
        f = open(args.f, 'r')
        r = f.read()
        f.close()
        recipients = r.splitlines()
    elif args.r != None:
        recipients = args.r

    src_count = 0
    try_count = 0
    ok_count = 0
    batch_size = config.get('AFRICASTALKING_BATCH_SIZE')
    batch = []
    for r in recipients:
        src_count += 1
        try:
            validate_phone(r)
        except NumberParseException:
            logg.error('phone number {} is invalid. skipping'.format(r))
            continue

        logg.debug('sending to {}'.format(r))
        batch.append(r)

        if src_count == len(recipients) or len(batch) == batch_size:
            try_count += len(batch)
            result = c.send(
                    args.message,
                    batch,
                    sender_id=sender_id,
                    )
            logg.info('AT response: {}'.format(result))
            ok_count += len(batch)
            batch = []
