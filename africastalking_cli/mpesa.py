# standard imports
import csv
import logging
import os
from csv import DictReader
from datetime import datetime

# third party imports
import africastalking

# local imports
from error.validations import process_phone_number, validate_amount, validate_currency

logg = logging.getLogger(__name__)


def process_args(argparser):
    argparser.add_argument('-f', '--recipient-file', dest='f', type=str, required=True,
                           help='load b2c transaction data (one recipient per line)')


def execute(config, args):
    b2c_client = africastalking.Payment
    processed_phone_numbers = []

    if args.f is not None:
        logg.debug('reading recipients from file {}'.format(args.f))
        with open(args.f, 'r') as f:
            phone_numbers = [row["phone_number"] for row in DictReader(f)]
            for i in range(len(phone_numbers)):
                phone_number = process_phone_number(phone_number=phone_numbers[i], region='KE')
                processed_phone_numbers.append(phone_number)
        with open(args.f, 'r') as f:
            amounts = [row["amount"] for row in DictReader(f)]
            for i in range(len(amounts)):
                validate_amount(amounts[i])

        with open(args.f, 'r') as f:
            currency_codes = [row["currency_code"] for row in DictReader(f)]
            for i in range(len(currency_codes)):
                validate_currency(currency_codes[i])

        with open(args.f, 'r') as f:
            metadata = [row["metadata"] for row in DictReader(f)]

        with open(args.f, 'r') as f:
            reasons = [row["reason"] for row in DictReader(f)]

        with open(args.f, 'r') as f:
            names = [row["name"] for row in DictReader(f)]

    batch = []
    for i in range(len(processed_phone_numbers)):
        formatted_metadata = metadata[i].split(',')
        metadata_dict = {formatted_metadata[i]: formatted_metadata[i + 1] for i in range(0, len(formatted_metadata), 2)}
        consumer = {
            'phoneNumber': processed_phone_numbers[i],
            'amount': str(amounts[i]),
            'currencyCode': currency_codes[i],
            'reason': reasons[i],
            'metadata': metadata_dict,
            'providerChannel': config.get('B2C_PROVIDER_CHANNEL')
        }
        if names[i] is not None:
            consumer['name'] = names[i]
        batch.append(consumer)

    # split into appropriate batch size:
    batch_size = int(config.get('AFRICASTALKING_BATCH_SIZE'))
    batch = [batch[i * batch_size:(i + 1) * batch_size] for i in range((len(batch) + batch_size - 1) // batch_size)]

    results = []
    for i in range(len(batch)):
        res = b2c_client.mobile_b2c(product_name=config.get('B2C_PRODUCT_NAME'), consumers=batch[i])
        results.append(res)

    failed_transactions = []
    queued_transactions = []
    total_transactions = []

    for result in results:

        # get entries
        entries = result.get("entries")

        # get failed transactions
        for entry in entries:
            if entry.get('errorMessage'):
                failed_transactions.append(entry)

            else:
                queued_transactions.append(entry)
            total_transactions.append(entry)

        # write cleaner csv for both transaction categories
        root_dir = os.path.dirname(os.path.dirname(__file__))
        log_files_path = os.path.join(f'{root_dir}/log')
        if not os.path.isdir(f'{root_dir}/log/'):
            os.mkdir(log_files_path)

        timestamp = datetime.now().strftime('%d_%m_%Y_%H_%M_%S_%f')

        with open(f'{log_files_path}/queued_transactions_{timestamp}.csv', 'w') as queued_transactions_file:
            """
            {
              "phoneNumber": "+2547XXXXXXXX",
              "provider": "Mpesa",
              "providerChannel": "XXXXXX",
              "status": "Queued",
              "transactionFee": "KES 0.1000",
              "transactionId": "ATPid_b12a5c385b60e38ffda40b4031f7c1fc",
              "value": "KES 10.0000"
            }
            """
            write = csv.writer(queued_transactions_file, delimiter=',')
            write.writerow(("recipient", "amount", "transaction_id", "transaction_fee"))
            for transaction in queued_transactions:
                write.writerow((
                    transaction.get("phoneNumber"),
                    transaction.get("value"),
                    transaction.get("transactionId"),
                    transaction.get("transactionFee")
                ))

        with open(f"{log_files_path}/failed_transactions_{timestamp}.csv", "w") as failed_transaction_file:
            """
            {
              "errorMessage": "Value is outside the allowed limits",
              "phoneNumber": "+254XXXXXXXX",
              "provider": "Mpesa",
              "providerChannel": "XXXXXX",
              "status": "InvalidRequest",
              "value": "KES 6.0000"
            }
            """
            write = csv.writer(failed_transaction_file, delimiter=',')
            write.writerow(("recipient", "amount", "status", "error_message"))
            for transaction in failed_transactions:
                write.writerow((
                    transaction.get("phoneNumber"),
                    transaction.get("value"),
                    transaction.get("status"),
                    transaction.get("errorMessage")
                ))

        # write csv with all transactions
        with open(f"{log_files_path}/total_transactions_{timestamp}.csv", "w") as total_transaction_file:
            write = csv.writer(total_transaction_file, delimiter=',')
            write.writerow(("recipient", "amount", "status"))
            for transaction in total_transactions:
                write.writerow((
                    transaction.get("phoneNumber"),
                    transaction.get("value"),
                    transaction.get("status")
                ))
