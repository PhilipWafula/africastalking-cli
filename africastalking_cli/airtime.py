# standard imports
import csv
import logging
import os
from csv import DictReader
from datetime import datetime

# third-party imports
import africastalking

# local imports
import error.validations as er

logg = logging.getLogger(__name__)


def process_args(argparser):
    argparser.add_argument('-f', '--recipient-file', dest='f', type=str, required=True,
                           help='load recipients from file (one number per line)')


def execute(config, args):
    atm = africastalking.Airtime
    processed_phone_numbers = []

    if args.f is not None:
        logg.debug('reading recipients from file {}'.format(args.f))
        with open(args.f, 'r') as f:
            phone_numbers = [row["phone_number"] for row in DictReader(f)]
            for i in range(len(phone_numbers)):
                phone_number = er.process_phone_number(phone_number=phone_numbers[i], region='KE')
                processed_phone_numbers.append(phone_number)

        with open(args.f, 'r') as f:
            amounts = [row["amount"] for row in DictReader(f)]
            for i in range(len(amounts)):
                er.validate_amount(amounts[i])

        with open(args.f, 'r') as f:
            currency_codes = [row["currency_code"] for row in DictReader(f)]
            for i in range(len(currency_codes)):
                er.validate_currency(currency_codes[i])

    batch = []
    for i in range(len(processed_phone_numbers)):
        code = {
            'phone_number': processed_phone_numbers[i],
            'amount': str(amounts[i]),
            'currency_code': currency_codes[i]
        }
        batch.append(code)

    results = []
    if len(batch) == len(processed_phone_numbers):
        for i in range(len(batch)):
            currency_code = batch[i]['currency_code']
            amount = str(batch[i]['amount'])
            phone = batch[i]['phone_number']
            idempotency_key = 'req-1234'
            res = atm.send(phone_number=phone, amount=amount, currency_code=currency_code,
                           idempotency_key=idempotency_key)
            results.append(res)

    failed_airtime = []
    queued_airtime = []
    total_airtime = []

    for result in results:

        # get response
        responses = result.get("responses")

        # get failed airtime
        for response in responses:
            if result.get('errorMessage') != "None":
                failed_airtime.append(response)

            else:
                queued_airtime.append(response)
            total_airtime.append(response)

    # write cleaner csv for both transaction categories
    root_dir = os.path.dirname(os.path.dirname(__file__))
    log_files_path = os.path.join(f'{root_dir}/log/')
    if not os.path.isdir(f'{root_dir}/log/'):
        os.mkdir(log_files_path)

    timestamp = datetime.now().strftime('%d_%m_%Y_%H_%M_%S_%f')

    with open(f'{log_files_path}sent_airtime_{timestamp}.csv', 'w') as queued_airtime_file:
        """
        {
          "phoneNumber": "+254706297812",
          "provider": "Mpesa",
          "providerChannel": "453501",
          "status": "Queued",
          "transactionFee": "KES 0.1000",
          "transactionId": "ATPid_b12a5c385b60e38ffda40b4031f7c1fc",
          "value": "KES 10.0000"
        }
        """
        write = csv.writer(queued_airtime_file, delimiter=',')
        write.writerow(("recipient", "amount", "request_id", "discount"))
        for transaction in queued_airtime:
            write.writerow((
                transaction.get("phoneNumber"),
                transaction.get("amount"),
                transaction.get("requestId"),
                transaction.get("discount")
            ))

    with open(f"{log_files_path}failed_airtime_{timestamp}.csv", "w") as failed_airtime_file:
        """
        {
          "errorMessage": "Value is outside the allowed limits",
          "phoneNumber": "+254725479377",
          "provider": "Mpesa",
          "providerChannel": "453501",
          "status": "InvalidRequest",
          "value": "KES 6.0000"
        }
        """
        write = csv.writer(failed_airtime_file, delimiter=',')
        write.writerow(("recipient", "amount", "status", "error_message"))
        for transaction in failed_airtime:
            write.writerow((
                transaction.get("phoneNumber"),
                transaction.get("amount"),
                transaction.get("status"),
                transaction.get("errorMessage")
            ))

    # write csv with all airtime
    with open(f"{log_files_path}total_airtime_{timestamp}.csv", "w") as total_airtime_file:
        write = csv.writer(total_airtime_file, delimiter=',')
        write.writerow(("recipient", "amount", "status"))
        for transaction in total_airtime:
            write.writerow((
                transaction.get("phoneNumber"),
                transaction.get("amount"),
                transaction.get("status")
            ))
