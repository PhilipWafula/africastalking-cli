# standard imports
import re

# third-party imports
import phonenumbers
from iso4217 import Currency


def validate_amount(value):
    return float(value)


def validate_currency(code):
    return Currency(code)


def validate_phone_number(phone_number: str):
    """This function checks that a phone number is valid
    :param phone_number: A phone number.
    :type phone_number: str
    :return: Validity of the phone number.
    :rtype: str
    """
    if re.match('[+]?[0-9]{10,12}$', phone_number):
        return True
    return False


def process_phone_number(phone_number: str, region: str):
    """This function parses any phone number for the provided region
    :param phone_number: A string with a phone number.
    :type phone_number: str
    :param region: Caller defined region
    :type region: str
    :return: The parsed phone number value based on the defined region
    :rtype: str
    """
    if not isinstance(phone_number, str):
        try:
            phone_number = str(int(phone_number))

        except ValueError:
            pass

    phone_number_object = phonenumbers.parse(phone_number, region)
    parsed_phone_number = phonenumbers.format_number(phone_number_object, phonenumbers.PhoneNumberFormat.E164)

    return parsed_phone_number