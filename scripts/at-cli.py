#!/usr/bin/python3

# Author: Louis Holbrook <dev@holbrook.no> (https://holbrook.no)
# Description: CLI frontend for Africas Talking APIs
# SPDX-License-Identifier: GPL-3.0-or-later

# standard imports
import os
import sys
import uuid
import json
import argparse
import logging
import urllib
from xdg.BaseDirectory import xdg_config_home
from urllib import request

# third-party imports
from confini import Config
import africastalking
from phonenumbers.phonenumberutil import NumberParseException

# local imports
import africastalking_cli.sms as cmd_sms
import africastalking_cli.airtime as cmd_airtime
import africastalking_cli.mpesa as cmd_mpesa

logging.basicConfig(level=logging.WARNING)
logg = logging.getLogger()

config_dir = os.path.join(xdg_config_home, 'at-cli')

argparser = argparse.ArgumentParser(description='CLI frontend for Africas Talking APIs')

baseparser = argparse.ArgumentParser(add_help=False)
baseparser.add_argument('-c', type=str, default=config_dir, help='config root to use')
baseparser.add_argument('-n', type=str, default='default', help='config context to use')
baseparser.add_argument('-v', help='be verbose', action='store_true')
baseparser.add_argument('-vv', help='be more verbose', action='store_true')

sub = argparser.add_subparsers()
sub.dest = 'command'
sub_sms = sub.add_parser('sms', help='send sms', parents=[baseparser])
cmd_sms.process_args(sub_sms)
sub_airtime = sub.add_parser('airtime', help='send airtime', parents=[baseparser])
cmd_airtime.process_args(sub_airtime)
sub_mpesa = sub.add_parser('mpesa', help='send money via b2c payments', parents=[baseparser])
cmd_mpesa.process_args(sub_mpesa)

args = argparser.parse_args(sys.argv[1:])

if args.v:
    logging.getLogger().setLevel(logging.INFO)
elif args.vv:
    logging.getLogger().setLevel(logging.DEBUG)

if args.command is None:
    logg.critical('Subcommand missing')
    sys.exit(1)

config_dir = os.path.join(args.c, args.n)
os.makedirs(config_dir, 0o777, True)

config = Config(config_dir)
config.process()
logg.debug('config loaded from {}'.format(config_dir))
logg.debug(config)

cmd_mod = None
subcmd = args.command
if subcmd == 'sms':
    cmd_mod = cmd_sms
elif subcmd == 'airtime':
    cmd_mod = cmd_airtime
elif subcmd == 'mpesa':
    cmd_mod = cmd_mpesa

# host = config.get('CLIENT_HOST')
# port = config.get('CLIENT_PORT')
# ssl = config.get('CLIENT_SSL')
#
# if host == None:
#    host = args.host
# if port == None:
#    port = args.port
# if ssl == None:
#    ssl = not args.nossl
# elif ssl == 0:
#    ssl = False
# else:
#    ssl = True


if __name__ == "__main__":

    africastalking.initialize(
        config.get('AFRICASTALKING_API_USER'),
        config.get('AFRICASTALKING_API_KEY'),
    )

    try:
        cmd_mod.execute(config, args)
    except ValueError as e:
        logg.error('{}'.format(e))
        sys.exit(1)

    except AttributeError as e:
        logg.error('{}'.format(e))
        sys.exit(1)

    except NumberParseException as e:
        logg.error('Invalid Phone Number : {}'.format(e))
        sys.exit(1)
